Rails.application.routes.draw do

  resources :blogs, except: [:index] do
    resources :posts, only: [:new, :create]
  end
  resources :posts, only: [:show, :edit, :update, :destroy]
  resources :comments, only: [:create, :destroy]
  devise_for :users
  resources :users, only: [:show, :edit, :update] do
    member do
      get :deactivate
    end
  end

  namespace :me do
    match 'stories/public', to: 'stories#public', as: :public_stories, via: :get
    match 'stories/drafts', to: 'stories#drafts', as: :drafts_stories, via: :get
    match 'blogs', to: 'blogs#index', as: :blogs, via: :get
  end

  authenticated :user do
    root to: 'home#index', as: 'home'
  end
  unauthenticated :user do
    root 'home#front'
  end

  match :top, to: 'home#top', as: :top, via: :get

  match :search, to: 'search#index', as: :search, via: :get

  match :like, to: 'likes#create', as: :like, via: :post
  match :unlike, to: 'likes#destroy', as: :unlike, via: :post

  match :bookmark, to: 'bookmarks#create', as: :bookmark, via: :post
  match :unbookmark, to: 'bookmarks#destroy', as: :unbookmark, via: :post
  match :bookmarks, to: 'bookmarks#index', as: :bookmarks, via: :get

  match :follow, to: 'follows#create', as: :follow, via: :post
  match :unfollow, to: 'follows#destroy', as: :unfollow, via: :post

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
