class AddCachedVotesToComments < ActiveRecord::Migration
  def change
    add_column :comments, :cached_scoped_like_votes_up, :integer, :default => 0
    add_index  :comments, :cached_scoped_like_votes_up
  end
end
