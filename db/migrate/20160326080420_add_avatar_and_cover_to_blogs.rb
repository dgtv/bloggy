class AddAvatarAndCoverToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :avatar, :string
    add_column :blogs, :cover, :string
  end
end
