var editor = new MediumEditor('.editable', {
  placeholder: {
      text: 'Tell your story...',
      hideOnClick: true
  }
});
$('.editable').bind('input propertychange', function() {
  $("#post_" + $(this).attr("data-field-id")).val($(this).html());
});