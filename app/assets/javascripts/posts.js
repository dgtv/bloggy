// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function() {
  $('#drafts-action').on('click', function() {
    document.getElementById("post_is_draft").checked = true;
    $('#new-post-form').submit();
  });
});

$(document).ready(function() {
  $('#publish-action').on('click', function() {
    document.getElementById("post_is_draft").checked = false;
    $('#new-post-form').submit();
  });
});