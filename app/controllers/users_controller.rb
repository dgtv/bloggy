class UsersController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_user
  before_action :check_ownership, only: [:edit, :update]

  def show
    @posts = @user.posts.where(is_draft: false)
                .paginate(page: params[:page], per_page: 10)
                .order(cached_scoped_like_votes_up: :desc)
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to user_path(@user)
    else
      render :edit
    end
  end

  def deactivate
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :about, :avatar)
    end

    def check_ownership
      redirect_to current_user unless @user == current_user
    end
end
