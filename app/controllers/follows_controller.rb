class FollowsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_followable
  respond_to :js

  def create
    current_user.follow @followable
  end

  def destroy
    current_user.stop_following @followable
  end

  private
    def find_followable
      @followable_type = params[:followable_type].classify
      @followable = @followable_type.constantize.find(params[:followable_id])
    end
end
