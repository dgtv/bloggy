class SearchController < ApplicationController
  def index
    @query = params[:query]
    @type = params[:type] || 'post'
    @searchable_type = @type.classify
    @results = @searchable_type.constantize.search(params[:query])
                  .paginate(page: params[:page], per_page: 10)
                  .order('created_at DESC')
  end
end
