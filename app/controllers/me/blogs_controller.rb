class Me::BlogsController < AuthOnlyController
  def index
    @user_blogs = current_user.blogs
    @following_blogs = current_user.following_by_type('Blog')
    @suggested_blogs = Blog.where.not(id: @following_blogs)
  end
end