class Me::StoriesController < AuthOnlyController
  respond_to :html, :js

  def public
    @stories = current_user.posts.where(is_draft: false)
                  .paginate(page: params[:page], per_page: 10)
                  .order('created_at DESC')
  end

  def drafts
    @stories = current_user.posts.where(is_draft: true)
                  .paginate(page: params[:page], per_page: 10)
                  .order('created_at DESC')
  end
end
