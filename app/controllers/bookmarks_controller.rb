class BookmarksController < ApplicationController
  before_action :authenticate_user!
  before_action :find_bookmarkable, except: [:index]
  respond_to :js, except: [:index]

  def index
    @bookmarks = current_user.find_liked_items vote_scope:'bookmark'
  end

  def create
    @bookmarkable.liked_by current_user, vote_scope: 'bookmark'
  end

  def destroy
    @bookmarkable.disliked_by current_user, vote_scope: 'bookmark'
  end

  private
    def find_bookmarkable
      @bookmarkable_type = params[:bookmarkable_type].classify
      @bookmarkable = @bookmarkable_type.constantize.find(params[:bookmarkable_id])
    end
end
