class HomeController < ApplicationController
  before_action :set_user, except: :front
  respond_to :html, :js

  def front
    @activities = PublicActivity::Activity.where(key: 'post.create')
                    .paginate(page: params[:page], per_page: 10)
                    .order(created_at: :desc)
  end

  def index
    following_blogs = @user.following_by_type('Blog')
    @activities = PublicActivity::Activity.where(recipient_id: following_blogs)
                    .paginate(page: params[:page], per_page: 10)
                    .order(created_at: :desc)
  end

  def top
    @activities = Post.where(is_draft: false)
                .order(cached_scoped_like_votes_up: :desc)
                .limit(20)
  end

  private
    def set_user
      @user = current_user
    end
end
