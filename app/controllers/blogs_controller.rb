class BlogsController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_blog, only: [:show, :edit, :update, :destroy]
  before_action :require_permission, only: [:edit, :destroy]

  def show
    @posts = @blog.posts
                .paginate(page: params[:page], per_page: 10)
                .order('created_at DESC')
  end

  def new
    @blog = Blog.new
  end

  def edit
  end

  def create
    @blog = current_user.blogs.new(blog_params)
    if @blog.save
      redirect_to @blog
    else
      redirect_to blogs_path, notice: @blog.errors.full_messages.first
    end
  end

  def update
    @blog.update(blog_params)
    redirect_to @blog, notice: 'Blog was successfully updated.'
  end

  def destroy
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to blogs_url, notice: 'Blog was successfully destroyed.' }
    end
  end

  private
    def set_blog
      @blog = Blog.find(params[:id])
    end

    def blog_params
      params.require(:blog).permit(:name, :description, :avatar, :cover)
    end

    def require_permission
      if @blog.user != current_user
        redirect_to blogs_path
      end
    end
end
