class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:show]

  before_action :set_post, only: [:edit, :update, :destroy]
  before_action :set_blog, only: [:new, :create]
  before_action :set_activity, only: [:update, :destroy]

  layout "post_actions", only: [:new, :edit]

  def show
    @post = Post.find(params[:id])

    if @post.is_draft && @post.user != current_user
      raise ActionController::RoutingError.new('Not Found')
    end

    @comments = @post.comments.all
                    .paginate(page: params[:page], per_page: 10)
                    .order('created_at DESC')
  end

  def new
    @post = Post.new
  end

  def edit
  end

  def create
    @post = @blog.posts.create(post_params) do |post|
      post.user = current_user
    end
    respond_to do |format|
      if @post.save
        create_post_activity unless @post.is_draft
        format.html { redirect_to @post,
                      notice: 'Post was successfully created.' }
      else
        format.html { render :new,
                      layout: 'post_actions',
                      notice: 'Error! Post was not created.' }
      end
    end
  end

  def update
    respond_to do |format|
      if @post.update(post_params)
        # manual activity creating/destroying
        create_post_activity unless @activity.present? || @post.is_draft
        @activity.destroy if @activity.present? && @post.is_draft
        format.html { redirect_to @post,
                      notice: 'Post was successfully updated.' }
      else
        format.html { render :edit,
                      notice: 'Error! Post was not updated.' }
      end
    end
  end

  def destroy
    blog = @post.blog
    @post.destroy
    respond_to do |format|
      format.html { redirect_to blog,
                    notice: 'Post was successfully destroyed.' }
    end
  end

  private

  def set_post
    @post = current_user.posts.find(params[:id])
  end

  def set_blog
    @blog = current_user.blogs.find(params[:blog_id])
  end

  def post_params
    params.require(:post).permit(:title, :content, :is_draft)
  end

  def set_activity
    @activity = PublicActivity::Activity
                  .find_by_trackable_id_and_key(@post.id, 'post.create')
  end

  def create_post_activity
    @post.create_activity(:create, owner: current_user, recipient: @post.blog)
  end
end
