class Post < ActiveRecord::Base
  before_destroy :find_and_destroy_activities

  belongs_to :blog
  belongs_to :user

  counter_culture :blog
  counter_culture :user

  validates_presence_of :content
  validates_presence_of :title
  validates_presence_of :user

  validates :title, length: { maximum: 100 }

  acts_as_commentable
  acts_as_votable

  include PublicActivity::Common

  def self.search(query)
    if query
      where("LOWER(title) LIKE ? AND is_draft = FALSE", "%#{query.downcase}%")
    else
      all()
    end
  end

  private
  def find_and_destroy_activities
    activities = PublicActivity::Activity.where(trackable_id: self.id, trackable_type: self.class.name)
    activities.each { |activity| activity.destroy if activity.present? }
  end
end
