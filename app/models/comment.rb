class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment

  belongs_to :commentable, :polymorphic => true, :counter_cache => true
  belongs_to :user

  acts_as_votable

  include PublicActivity::Common

  validates_presence_of :comment
  validates_presence_of :commentable
  validates_presence_of :user
end
