class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, :trackable, :validatable

  acts_as_voter
  acts_as_follower

  has_many :blogs, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy

  mount_uploader :avatar, AvatarUploader

  validates_presence_of :name

  def self.search(query)
    if query
      where("LOWER(name) LIKE ?", "%#{query.downcase}%")
    else
      all()
    end
  end
end
