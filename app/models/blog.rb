class Blog < ActiveRecord::Base
  belongs_to :user
  has_many :posts, dependent: :destroy

  acts_as_followable

  mount_uploader :avatar, AvatarUploader
  mount_uploader :cover, AvatarUploader

  validates_presence_of :name
  validates :description, length: { maximum: 250 }

  include PublicActivity::Common

  def self.search(query)
    if query
      where("LOWER(name) LIKE ?", "%#{query.downcase}%")
    else
      all()
    end
  end
end
