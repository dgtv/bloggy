## Bloggy!
Bloggy is an open source social blogging platform inspired by [Medium](https://medium.com/) and written in Ruby on Rails.

Do you want to see it in action? Here is a working version deployed to heroku [dgt-bloggy](https://dgt-bloggy.herokuapp.com/)

### What it uses?
* [Ruby 2.2.4](https://www.ruby-lang.org/)
* [Ruby on Rails 4.2.3](https://github.com/rails/rails)
* [PostgreSQL](http://www.postgresql.org/)
* [Puma](http://puma.io/)
* [Devise](https://github.com/plataformatec/devise)
* [Public Activity](https://github.com/chaps-io/public_activity)
* [Carrierwave](https://github.com/carrierwaveuploader/carrierwave) and [Cloudinary](http://cloudinary.com/) on production
* [Bootstrap](http://getbootstrap.com/) and [Font Awesome](https://fortawesome.github.io/Font-Awesome/)
* [MediumEditor](http://yabwe.github.io/medium-editor/)

### How do I get set up?
To set it up on your local machine here is what you need to do. Install Ruby & Rails. Clone this repo using the following command:

```
git clone https://github.com/dgtv/bloggy.git
cd bloggy
```
Then resolve dependencies using bundler:

```
bundle install
```

Run Migrations:

```
rake db:migrate
```

Run rails using

```
rails server
```

### Features
* User can create blogs
* Public and Draft posts (drafts can see only owner)
* Posts commenting
* Simple search by posts, users and blogs
* Subscribing to blogs and see their updates on home page
* Top posts list by likes
* Posts bookmarking
* Posts and comments likes
* Unauthorized users also can view posts, comments, users, the top list and use search
* Pagination with endless scroll
* Responsive and user friendly UI
* WYSIWYG editor
