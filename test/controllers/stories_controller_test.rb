require 'test_helper'

class StoriesControllerTest < ActionController::TestCase
  test "should get public" do
    get :public
    assert_response :success
  end

  test "should get drafts" do
    get :drafts
    assert_response :success
  end

end
